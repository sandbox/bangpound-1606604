/*global Drupal,jQuery */
(function ($) {

  Drupal.behaviors.drippyMessages = {
    attach: function (context, settings) {
      $('#block-drippy-assistant', context).once('drippyMessages', function () {
        var $container = $(this),
          $assistant = $('div.content', this);

        Messenger = {
          messages: [],
          addMessage: function (data) {
            this.messages.push(data);
            this.showNext();
          },
          show: function () {
            $assistant.is(':not(:visible)') ? this.toggle() : null;
          },
          hide: function () {
            $assistant.is(':visible') ? this.toggle() : null;
          },
          toggle: function () {
            $assistant.toggle('fade', { }, 'slow', function () {
              if ($(this).is(':visible')) {
                $container.addClass('drippy-visible');
              }
              else {
                $container.removeClass('drippy-visible');
              }
            });
          },
          showNext: function () {
            var message = this.messages.shift();
            Messenger.show();
            $assistant.delay(message.delay);
            $assistant.queue(function () {
              Drupal.theme('drippyMessage', message)
                .hide()
                .insertBefore($assistant)
                .fadeIn()
                .delay(message.delay)
                .fadeOut();

              $assistant.dequeue();
            });
          }
        };

        //Messenger.toggle();
        $(this).click(function () {
          Messenger.toggle();
        });

        $.each(settings.drippyMessages, function (index, data) {
          Messenger.addMessage(data);
        });
      });
    },

    detach: function (context, settings, trigger) {
    }
  };

  Drupal.theme.prototype.drippyMessage = function (data) {
    return $('<div/>', {
      "class": 'drippy-message drippy-' + data.type
    }).html('<div class="inner">' + data.message + '</div>');
  };

}(jQuery));
