<?php

/**
 * Implementation of hook_block_info().
 */
function drippy_block_info() {
  $blocks['assistant'] = array(
    'info' => t('Assistant'),
    'cache' => DRUPAL_NO_CACHE,
  );

  return $blocks;
}


/**
 * Implementation of hook_block_info().
 */
function drippy_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'assistant':
      $block['subject'] = t('Drippy');
      $block['content'] = array(
        '#theme' => 'image',
        '#path' => drupal_get_path('module', 'drippy') .'/images/assistant.png',
        '#alt' => 'Drippy Assistant',
        '#pre_render' => array('drippy_attach_messages'),
        '#attached' => array(
          'library' => array(array('system', 'effects.fade')),
          'js' => array(
            drupal_get_path('module', 'drippy') . '/drippy.js',
            libraries_get_path('jquery-tmpl') .'/jquery.tmpl.min.js',
          ),
          'css' => array(drupal_get_path('module', 'drippy') . '/drippy.css'),
        ),
      );
      break;
  }
  return $block;
}

/**
 * Implements hook_action_info().
 */
function drippy_action_info() {
  return array(
    'drippy_message_action' => array(
      'type' => 'drippy',
      'label' => t('Display a message to the user through the Drippy Assistant'),
      'configurable' => TRUE,
      'triggers' => array('any'),
    ),
  );
}

function drippy_message_action_form($context) {
  $form['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#default_value' => isset($context['message']) ? $context['message'] : '',
    '#required' => TRUE,
    '#rows' => '8',
    '#description' => t('The message to be displayed to the current user. You may include placeholders like [node:title], [user:name], and [comment:body] to represent data that will be different each time message is sent. Not all placeholders will be available in all contexts.'),
  );
  return $form;
}

function drippy_message_action_submit($form, $form_state) {
  return array('message' => $form_state['values']['message']);
}

/**
 * Sends a message to the current user's screen.
 *
 * @param object $entity
 *   An optional node object, which will be added as $context['node'] if
 *   provided.
 * @param array $context
 *   Array with the following elements:
 *   - 'message': The message to send. This will be passed through
 *     token_replace().
 *   - Other elements will be used as the data for token replacement in
 *     the message.
 *
 * @ingroup actions
 */
function drippy_message_action(&$entity, $context = array()) {
  if (empty($context['node'])) {
    $context['node'] = $entity;
  }

  $context['message'] = token_replace(filter_xss_admin($context['message']), $context);
  drippy_add_messages($context['message']);
}

/**
 * function to add a message to drippy's output.
 */
function drippy_add_messages($message = NULL, $type = 'status', $repeat = TRUE) {
  $messages = &drupal_static(__FUNCTION__, array());
  if ($message) {
    $messages[] = array(
      'message' => $message,
      'type' => $type,
      'repeat' => $repeat,
      'delay' => 5000,
    );
  }
  return $messages;
}

/**
 * Post render function to attach messages to block.
 */
function drippy_attach_messages($element) {
  $element['#attached']['js'][] = array(
    'type' => 'setting',
    'data' => array('drippyMessages' => drippy_add_messages()),
  );
  return $element;
}
